# Desafio Backend Developer #

Conforme proposta apresentada segue solução desenvolvida. 

Utilizado NodeJS como servidor de aplicação, junto com os seguintes frameworks:

 - Express: Criação da API Rest 

 - mongoose: ORM para persistencia no banco de dados mongodb. 

 - config: Biblioteca para leitura de arquivos de configuração. 

 - pagarme: Integração com API de pagamento pagar.me. 

 - swagger-ui-express: Ferramenta para documentar a API Rest. 


Para persistencia de dados foi utilizado o banco de dados MongoDB.

##### Pré-requisitos 

Antes de inicializar é necessário instalar as seguintes ferramentas:

 - NodeJS, desenvolvido na versão 8.11.4

 - Mongodb

##### Inicialização do projeto  

Execute o seguinte comando na pasta do projeto:

```
npm install
```

Execute o comando para inicializar o banco de dados

```
mongod --port 27017 --dbpath c:\desafio-backend\banco
```

* O diretório utilizado no comando é apenas um exemplo, utilizar o diretório adequedo ao seu ambiente.

* O executável 'mongod' deve estar nas variáveis de ambiente do sistema, caso não esteja, posicione o command na pasta bin da instalação do mongodb, por exemplo:

```
cd "C:\Program Files\MongoDB\Server\3.6\bin"
```

Por fim, basta executar o app.js, após isso a API estará disponível.
```
node app.js
```


##### Estrutura do projeto 

```
    .
    ├── ...
    ├── app
    │   ├── controller
    │   ├── lib
    │   ├── model
    │   ├── route
    │   ├── utils
    │   └── setup.js
    ├── config
    ├── test
    ├── app.js
    └── ...
```

 - app/controller: Camada de controller que irá tratar as regras do negócio.

 - app/lib: Implementações específicar para funcionamento do projeto, por exemplo configuração do express para API Rest, conexão com banco de dados...

 - app/model: Modelos das entidades que será mapeado no banco de dados e as regras de validações pertinentes.

 - app/route: Rotas por entidade que estará disponível na API Rest.

 - app/utils: Código para operação pontual que pode ser compartilhado dentro da aplicação.

 - app/setup.js: Código responsável pela inicialização do aplicativo.

 - config: Arquivo de configuração com parametros do sistema.

 - teste: Implementação do teste unitário (não concluído).

 - app.js: Ponto de inicialização do aplicativo.

##### Definição da API 

O projeto utiliza a bliblioteca swagger para documentação da API. Para visualizar basta inicializar o projeto e acessar a seguinte URL:

```
http://localhost:8080/api-docs
```

A API esta disponível na seguinte URL

```
http://localhost:8080/api/***
```

Para garantir a segurança no acesso aos dados foi implementado um mecanismo de autenticação básico (Basic authentication).

Os end-points com esse requisito devem incluir no http header o seguinte parametro:

```
Authorization: Basic <base64>=
```

Onde \<base64>= deve ser o encode em Base64 da seguinte string:
usuario:senha

##### API  

#### Pagamento

- A API de pagamento dever ser acessar pelo endpoint:

```
POST http://localhost:8080/api/carrinhoCompra/id_carrinho/pagar
```

 - 'id_carrinho' deve ser alterado pelo id do carrinho que deseja pagar.

 - Caso a API retorne o status "PAID", o carrinho será atualizado.

 - Não é necessário um payload.

#### Usuário 

![alt text](./doc/get-usuario.png "GET Usuário")
![alt text](./doc/post-usuario.png "POST Usuário")
![alt text](./doc/del-usuario.png "DELETE Usuário")
![alt text](./doc/put-usuario.png "PUT Usuário")

#### Produto 

![alt text](./doc/get-produto.png "GET Produto")
![alt text](./doc/post-produto.png "POST Produto")
![alt text](./doc/del-produto.png "DELETE Produto")
![alt text](./doc/put-produto.png "PUT Produto")

#### Carrinho de compra 

![alt text](./doc/get-carrinho.png "GET Carrinho")
![alt text](./doc/put-carrinho.png "PUT Carrinho")
![alt text](./doc/get-carrinho-id.png "GET Carrinho/{id}")

#### Item do carrinho de compra 

![alt text](./doc/get-carrinho-item.png "GET Item")
![alt text](./doc/post-carrinho-item.png "POST Item")
![alt text](./doc/del-carrinho-item.png "DEL Item")

##### Sugestão de sequencia de chamadas para teste

1) POST /usuario

    1.1) Primeiramente é necessário criar um usuário para realizar o acesso da API.

    1.2) Com o usuario criado a chave de acesso deve ser gerada, basta realizar o encode base64 da string "usuario:senha".

    1.3) Como exemplo, para o usuario admin senha admin a chave é: "YWRtaW46YWRtaW4="

2) POST /produto

    2.1) Será necessário cadastrar os produtos a serem utilizados.
    2.2) Caso queira consultar os produtos já cadastrados basta acessar a url GET /produto.

3) POST /carrinhocompraitem

    3.1) Não é necessário criar um carrinho pendente, basta inserir um produto e o carrinho será criado.

4) GET /carrinhocompra/id

    4.1) Irá apresentar o registro de carrinho completo, descrevendo os itens e valores.

5) POST /carrinhocompra/id/pagar

    5.1) Irá enviar o objeto com os dados do pagamento para a api PagarME.

    5.2) Caso o processo ocorra com sucesso irá atualizar o carrinho para "Faturado".

##### Tarefas futuras

 - Em função do prazo disponível e esforço empregado segue algumas atividades pendentes:

    - Implementação TDD, iniciei o desenvolvimento utilizando o framework Mocha, porém removi o codigo do projeto pois não tive tempo habil para concluir.

    - Integração com docker, utilizando o componente docker-compose.

##### Considerações finais

 - Criei o mecanismo de usuário a fim de melhor a segurança da aplicação, também restringindo o acesso ao carrinho de compra.

 - Esse projeto foi desenvolvido/documentado em aproximadamente 6 horas.
