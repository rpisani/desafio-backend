const config = require('config');

const api = require('./lib/api');
const mongodb = require('./lib/mongodb');

const route = require('./route');

const initDb = () => {
    let cfg = config.get('db');
    return mongodb.connect(cfg);
};

const initApi = () => {
    let cfg = config.get('api');

    let routes = route(api.app);
    api.use(cfg.baseurl, routes);
    
    api.listen(cfg);
};

const init = async () => {
    await initDb();
    initApi();
};

module.exports = {
    init: init
};
