const express = require('express');
const auth = require('./auth');
const app = express();
const bodyParser = require('body-parser');

const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('../../config/swagger.json');

app.use(auth);
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

const listen = (config) => {
    let port = config.port;
    app.listen(port, () => {
        console.log('Server is listening on port ' + port);
    });

};

const use = (baseUrl, routes) => {
    app.use(baseUrl, routes);
}

module.exports = {
    listen: listen,
    use: use
};
