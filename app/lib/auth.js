const auth = require('basic-auth');
const apiUtils = require('../utils/apiUtils');
const Usuario = require('../model/Usuario');

const verifyAuth = async function (request, response, next) {
    try {
        let url = request.originalUrl;
        if ((url.indexOf('usuario') >= 0) && (request.method == 'POST')) {
            return next();
        }
        if (url.indexOf('api-docs') >= 0) {
            return next();
        }

        let user = auth(request);
        let data = await Usuario.findOne({Login: user.name, Senha: user.pass});

        if (!data) {
            apiUtils.send401(response);
            return;
        }
        
        request.user = data;
        return next();
    } catch (err) {
        apiUtils.send401(response);
    }
};

module.exports = verifyAuth;