const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

const connect = (config) => {
    return mongoose.connect(config.url, {
        useNewUrlParser: true
    });
};

module.exports = {
    connect: connect
};
