const mongoose = require('mongoose');
const consts = require('../utils/consts');

const ProdutoSchema = mongoose.Schema(
    {
        Codigo: {
            type: String,
            required: [true, 'Código do produto obrigatório.']
        },
        Nome: {
            type: String,
            required: [true, 'Nome do produto obrigatório.']
        },
        Descricao: {
            type: String
        },
        Imagem: {
            type: String
        },
        Valor: {
            type: Number,
            required: [true, 'Valor do produto obrigatório.']
        },
        Fator: { 
            type: String, 
            enum: consts.FATOR
        }
    }, 
    {
        timestamps: true,
        toJSON: { virtuals: true } 
    }
);

module.exports = mongoose.model('Produto', ProdutoSchema);