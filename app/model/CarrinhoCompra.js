const mongoose = require('mongoose');
const consts = require('../utils/consts');
const Schema = mongoose.Schema;

const CarrinhoCompraSchema = mongoose.Schema(
    {
        Usuario: { type: Schema.Types.ObjectId, ref: 'Usuario' },
        Situacao: { 
            type: String, 
            enum: consts.SITUACAO_CARRINHO
        }
    }, 
    {
        timestamps: true,
        toJSON: { virtuals: true } 
    }
);

CarrinhoCompraSchema.virtual('Itens', {
    ref: 'CarrinhoCompraItem',
    localField: '_id', 
    foreignField: 'CarrinhoCompra', 
});
  
CarrinhoCompraSchema.virtual('ValorProduto').get(function() {  
    let vl = 0;
    this.Itens.forEach(el => {
        vl += el.ValorBruto || 0;
    });
    return vl;
});

CarrinhoCompraSchema.virtual('ValorTotal').get(function() {  
    let vl = 0;
    this.Itens.forEach(el => {
        vl += el.ValorLiq || 0;
    });
    return vl;
});

module.exports = mongoose.model('CarrinhoCompra', CarrinhoCompraSchema);