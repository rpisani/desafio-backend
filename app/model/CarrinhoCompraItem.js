const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CarrinhoCompraItemSchema = mongoose.Schema(
    {
        Produto: { type: Schema.Types.ObjectId, ref: 'Produto' },
        CarrinhoCompra: { type: Schema.Types.ObjectId, ref: 'CarrinhoCompra' },
        DescontoPerc: {
            type: Number
        },
        Quantidade: {
            type: Number
        }
    }, 
    {
        timestamps: true,
        toJSON: { virtuals: true } 
    }
);

CarrinhoCompraItemSchema.virtual('ValorBruto').get(function() {  
    return (this.Produto.Valor * this.Quantidade);
});


CarrinhoCompraItemSchema.virtual('ValorLiq').get(function() {  
    return ((this.Produto.Valor) * ((1 - (this.DescontoPerc / 100)) || 1) * this.Quantidade);
});

module.exports = mongoose.model('CarrinhoCompraItem', CarrinhoCompraItemSchema);