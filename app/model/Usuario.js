const mongoose = require('mongoose');

const UsuarioSchema = mongoose.Schema(
    {
        Login: {
            type: String,
            required: [true, 'Login obrigatório.']
        },
        Senha: {
            type: String,
            required: [true, 'Senha obrigatória.']
        }
    }, 
    {
        timestamps: true
    }
);

module.exports = mongoose.model('Usuario', UsuarioSchema);