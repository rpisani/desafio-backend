const validateExcept = require('./validateExcept');
const consts = require('../utils/consts');

const send = (res, data) => {
    if (res) {
        res.send(data);
    }
    return data;
}

const send500 = (res, msg) => {
    if (res) {
        res.status(500).send({
            message: msg
        });
    }
    return false;
}

const send404 = (res, msg) => {
    if (res) {
        res.status(404).send({
            message: msg
        });
    }
    return false;
}

const send401 = (res) => {
    if (res) {
        res.set('WWW-Authenticate', 'Basic realm="desafio-backend"');
        res.status(401).send(); 
    }
    return false;
}

const send400 = (res, msg) => {
    if (res) {
        res.status(400).send({
            message: msg
        });
    }
    return false;
}

const registroValido = (data) => {
    if (data) return true;
    else {
        throw new validateExcept(null, 404);
    }
}

const modelValido = (data) => {
    let erro = data.validateSync();
    if (erro) {
        throw new validateExcept(erro, 400);
    } else {
        return true;
    }
}


module.exports = {
    send: send,
    send500: send500,
    send400: send400,
    send401: send401,
    send404: send404,
    registroValido: registroValido,
    modelValido: modelValido
}