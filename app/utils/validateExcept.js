module.exports = function validateExcept(err, code) {
    Error.captureStackTrace(this, this.constructor);
    this.name = this.constructor.name;
    this.message = err;
    this.code = code;
  };
  
  require('util').inherits(module.exports, Error);