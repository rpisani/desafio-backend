
const UsuarioAPI = (router, controller) => {
    let nameCtrl = 'Usuario';

    router.get('/usuario', (req, res) => controller.call(req, res, nameCtrl, 'findAll'));
    router.post('/usuario', (req, res) => controller.call(req, res, nameCtrl, 'create'));
    router.put('/usuario/:_id', (req, res) => controller.call(req, res, nameCtrl, 'update'));
    router.delete('/usuario/:_id', (req, res) => controller.call(req, res, nameCtrl, 'destroy'));
};

module.exports = UsuarioAPI;