
const CarrinhoCompraItemAPI = (router, controller) => {
    let nameCtrl = 'CarrinhoCompraItem';

    router.get('/carrinhoCompraItem', (req, res) => controller.call(req, res, nameCtrl, 'findAll'));
    router.post('/carrinhoCompraItem', (req, res) => controller.call(req, res, nameCtrl, 'create'));
    router.delete('/carrinhoCompraItem/:_id_item', (req, res) => controller.call(req, res, nameCtrl, 'destroyItem'));

};

module.exports = CarrinhoCompraItemAPI;