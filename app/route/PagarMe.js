
const PagarMeAPI = (router, controller) => {
    let nameCtrl = 'PagarMe';

    router.post('/carrinhoCompra/:_id/pagar', (req, res) => controller.call(req, res, nameCtrl, 'pagar'));
};

module.exports = PagarMeAPI;