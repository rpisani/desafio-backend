
const ProdutoAPI = (router, controller) => {
    let nameCtrl = 'Produto';

    router.get('/produto', (req, res) => controller.call(req, res, nameCtrl, 'findAll'));
    router.get('/produto/:_id', (req, res) => controller.call(req, res, nameCtrl, 'findOne'));
    router.post('/produto', (req, res) => controller.call(req, res, nameCtrl, 'create'));
    router.put('/produto/:_id', (req, res) => controller.call(req, res, nameCtrl, 'update'));
    router.delete('/produto/:_id', (req, res) => controller.call(req, res, nameCtrl, 'destroy'));

};

module.exports = ProdutoAPI;