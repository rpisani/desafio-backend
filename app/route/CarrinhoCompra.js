
const CarrinhoCompraAPI = (router, controller) => {
    let nameCtrl = 'CarrinhoCompra';

    router.get('/carrinhoCompra', (req, res) => controller.call(req, res, nameCtrl, 'findAll'));
    router.get('/carrinhoCompra/:_id', (req, res) => controller.call(req, res, nameCtrl, 'findOne'));
    router.post('/carrinhoCompra', (req, res) => controller.call(req, res, nameCtrl, 'create'));
};

module.exports = CarrinhoCompraAPI;