const express = require('express');
const router = express.Router();
const controller = require('../controller');

const getRouter = () => {
    require('fs').readdirSync('./app/route').forEach((file) => {
        if (file !== 'index.js') {
            file = file.replace('.js', '');
            route = require('./' + file);
            route(router, controller);
        }
    });
    return router;
}

module.exports = getRouter;
