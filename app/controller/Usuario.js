const apiUtils = require('../utils/apiUtils');
const Usuario = require('../model/Usuario');

const create = async (param) => {
    const usuario = new Usuario({
        Login: param.payload.Login,
        Senha: param.payload.Senha
    });

    if (!apiUtils.modelValido(usuario)) return;

    let data = await usuario.save();
    return data;
}

const findAll = async (param) => {
    let id = param.user.id;
    let data = await Usuario.find({_id: id});
    return data;
}

const update = async (param) => {
    const usuario = {
        Login: param.payload.Login,
        Senha: param.payload.Senha
    };

    if (!apiUtils.modelValido(new Usuario(usuario))) return;

    let id = param.user.id;
    let data = await Usuario.findOneAndUpdate(
        { _id: id }, 
        usuario, 
        { new: true }
    );
    if (!apiUtils.registroValido(usuario)) return;

    return data;
}

const destroy = async (param) => {
    let id = param.user.id;
    let data = await Usuario.findByIdAndRemove(id);
    if (!apiUtils.registroValido(data)) return;
    return data;
}

module.exports = {
    create: create,
    findAll: findAll,
    update: update,
    destroy: destroy
};
