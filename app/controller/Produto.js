const apiUtils = require('../utils/apiUtils');
const consts = require('../utils/consts');
const Produto = require('../model/Produto');

const create = async (param) => {
    const produto = new Produto({
        Codigo: param.payload.Codigo, 
        Nome: param.payload.Nome,
        Descricao: param.payload.Descricao,
        Imagem: param.payload.Imagem,
        Valor: param.payload.Valor,
        Fator: param.payload.Fator
    });

    if (!apiUtils.modelValido(produto)) return;

    let data = await produto.save();
    return data;
}

const findAll = async (param) => {
    let data = await Produto.find();
    return data;
}

const findOne = async (param) => {
    let produto = await Produto.findById(param.param._id);
    if (!apiUtils.registroValido(produto)) return;
    return produto;
}

const update = async (param) => {
    const produto = new Produto({
        _id: param.payload._id,
        Codigo: param.payload.Codigo,
        Nome: param.payload.Nome,
        Descricao: param.payload.Descricao,
        Imagem: param.payload.Imagem,
        Valor: param.payload.Valor,
        Fator: param.payload.Fator
    });

    if (!apiUtils.modelValido(produto)) return;
    
    let data = await Produto.findOneAndUpdate(
        { _id: param.param._id }, 
        produto, 
        { new: true }
    );
    if (!apiUtils.registroValido(produto)) return;
    return data;
}

const destroy = async (param) => {
    let data = await Produto.findByIdAndRemove(param.param._id);
    if (!apiUtils.registroValido(data)) return;
    return data;
}

module.exports = {
    create: create,
    findAll: findAll,
    findOne: findOne,
    update: update,
    destroy: destroy
};
