const apiUtils = require('../utils/apiUtils');
const consts = require('../utils/consts');
const Produto = require('../model/Produto');
const CarrinhoCompraItem = require('../model/CarrinhoCompraItem');
const CarrinhoCompraCtrl = require('./CarrinhoCompra');

const create = async (param) => {
    let id_usuario = param.user.id;
    let id_carrinho = param.payload.CarrinhoCompra;
    let id_produto = param.payload.Produto;
    let quantidade = param.payload.Quantidade;

    if (!id_carrinho) {
        id_carrinho = await encontraOuCriaCarrinhoPendente(id_usuario);
    }

    let data = await CarrinhoCompraItem.findOne({"CarrinhoCompra": id_carrinho, "Produto": id_produto});
    if (data) {
        data = await CarrinhoCompraItem.populate(data, 'Produto');
    } else {
        data = new CarrinhoCompraItem({
            CarrinhoCompra: id_carrinho,
            Produto: await Produto.findOne({_id: id_produto}),
            Quantidade: 0
        });
    }
    data.Quantidade += quantidade;

    calculaDesconto(data);

    if (!apiUtils.modelValido(data)) return;

    await data.save();
    return data;
}

const findAll = async (param) => {
    let id_usuario = param.user.id;
    let id_carrinho = param.payload.carrinho;

    if (!id_carrinho) {
        id_carrinho = await encontraOuCriaCarrinhoPendente(id_usuario);
    }

    let data = await CarrinhoCompraItem.find({'CarrinhoCompra': id_carrinho});
    data = await CarrinhoCompraItem.populate(data, 'Produto');
    return data;
}

const destroyItem = async (param) => {
    let id_item = param.param._id_item;
    let data = await CarrinhoCompraItem.findByIdAndRemove(id_item);
    if (!apiUtils.registroValido(data)) return;
    return data;
}

const encontraOuCriaCarrinhoPendente = async (id_usuario) => {
    let reg = await CarrinhoCompraCtrl.findByFilter({'Usuario': id_usuario, 'Situacao': consts.SITUACAO_CARRINHO[0]});
    if (reg && reg.length) reg = reg[0];
    else reg = null;
    
    if (reg) {
        return reg.id
    } else {
        reg = await CarrinhoCompraCtrl.create({
            user: {id: id_usuario}
        });
        return reg.id;
    }
}

const calculaDesconto = (data) => {

    //
    // Calcula o desconto e aplica em cada um dos itens
    //
    let descperc = 0;
    switch (data.Produto.Fator) {
        case "A": 
            descperc = data.Quantidade * 1
            if (descperc > 5) descperc = 5;
            break;
        case "B": 
            descperc = data.Quantidade * 5
            if (descperc > 15) descperc = 15;
            break;
        case "C": 
            descperc = data.Quantidade * 10
            if (descperc > 30) descperc = 30;
            break;
        default:
            descperc = 0;
    }
    data.DescontoPerc = descperc;
}

module.exports = {
    create: create,
    findAll: findAll,
    destroyItem: destroyItem
};
