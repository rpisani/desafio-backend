const controllers = {};
const apiUtils = require('../utils/apiUtils');

require('fs').readdirSync('./app/controller').forEach((file) => {
    if (file !== 'index.js') {
        file = file.replace('.js', '');
        controllers[file] = require('./' + file);
    }
});

const call = async (req, res, nameCtrl, methodCtrl) => {
    try {
        let param = {
            user: req.user,
            payload: req.body,
            param: req.params
        };
        let data = await controllers[nameCtrl][methodCtrl](param);    
        apiUtils.send(res, data);

    } catch (err) {
        if (err.code == '400') {
            apiUtils.send400(res, err.message);
        } else if (err.code == '404') {
            apiUtils.send404(res, err.message)
        } else{
            apiUtils.send500(res, err.message);
        }
    }
}

module.exports = {
    call: call
};
