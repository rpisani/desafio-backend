const pagarme = require('pagarme');
const consts = require('../utils/consts');
const pagarMeUtils = require('../utils/pagarMeUtils');
const carrinhoCompraCtrl = require('./CarrinhoCompra');

const pagar = async (param) => {
    let id_carrinho = param.param._id;
    let id_usuario = param.user.id;
    let carrinho = await carrinhoCompraCtrl.findOne({
        user: {id: id_usuario},
        param: {_id: id_carrinho}
    });

    let client = await pagarme.client.connect(
        { api_key: consts.PAGAR_ME_APIKEY }
    );
    
    let data = pagarMeUtils.parse(carrinho);
    data = await client.transactions.create(data);

    if (data.status == 'paid') {
        carrinho.Situacao = consts.SITUACAO_CARRINHO[1];
        carrinhoCompraCtrl.put({
            payload: carrinho,
            param: {_id: id_carrinho}
        })
    }

    return data;
}

module.exports = {
    pagar: pagar
}