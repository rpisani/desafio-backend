const apiUtils = require('../utils/apiUtils');
const consts = require('../utils/consts');
const CarrinhoCompra = require('../model/CarrinhoCompra');
const CarrinhoCompraItem = require('../model/CarrinhoCompraItem');

const create = async (param) => {
    let id_usuario = param.user.id;
    carrinhoCompra = new CarrinhoCompra({
        Usuario: id_usuario,
        Situacao: consts.SITUACAO_CARRINHO[0]
    });

    if (!apiUtils.modelValido(carrinhoCompra)) return;

    let data = await carrinhoCompra.save();
    return data;
}

const put = async (param) => {
    carrinhoCompra = {
        Usuario: param.payload.Usuario,
        Situacao: param.payload.Situacao
    };

    if (!apiUtils.modelValido(new CarrinhoCompra(carrinhoCompra))) return;
    
    let data = await CarrinhoCompra.findOneAndUpdate(
        { _id: param.param._id }, 
        carrinhoCompra, 
        { new: true }
    );
    if (!apiUtils.registroValido(carrinhoCompra)) return;
    return data;
}

const findAll = async (param) => {
        let id_usuario = param.user.id;
        let data = await CarrinhoCompra.findOne({"Usuario": id_usuario});

        data = await CarrinhoCompra.populate(data, 'Itens');
        if (data) {
            data.Itens = await CarrinhoCompraItem.populate(data.Itens, 'Produto');
        }

        return data;
}

const findOne = async (param) => {
    let id_usuario = param.user.id;
    id_carrinho = param.param._id;

    let data = await CarrinhoCompra.findOne({"Usuario": id_usuario, "_id": id_carrinho});
    
    data = await CarrinhoCompra.populate(data, 'Itens');
    if (data) {
        data.Itens = await CarrinhoCompraItem.populate(data.Itens, 'Produto');
    }

    return data;
}

const findByFilter = (filter) => {
    return CarrinhoCompra.find(filter);
}

module.exports = {
    create: create,
    put: put,
    findAll: findAll,
    findOne: findOne,
    findByFilter: findByFilter
};
